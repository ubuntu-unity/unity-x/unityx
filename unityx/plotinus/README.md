# Plotinus HUD

Plotinus (based on a project from 2017 with the same name) is a modern
alternative to Unity7's HUD and mate-hud for UnityX.

## Building from source

**Dependencies:**

* `git`
* `cmake`
* `vala` (`valac` on Debian-based distributions such as Ubuntu and Devuan)
* `libgtk-3-dev` (`gtk3-devel` on Rocky, Fedora, RHEL etc.)

**Building and installing:**

`mkdir build && cd build`

`cmake ..`

`make -j$(nproc)` (or `make -j(nproc)` on Fish)

`make install` as root
